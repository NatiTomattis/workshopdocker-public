# Diferencias entre ADD y COPY

- ADD permite descargar carpetas y archivos desde URL's
- ADD permite descomprimir archivos gzip, bzip2 o xz
- COPY permite recibir archivos de otro contenedor cuando el Dockerfile describe un *multi-stage build* (lo vemos mas adelante) con la flag `--from`
- COPY y ADD solo pueden copiar archivos del host que estan en el *build context*
- Ambos permiten cambiar los permisos de usuario al copiar la carpetas

# Ayudita
- ADD https://gist.githubusercontent.com/NatiTomattis/65e5e03a0fe739586f9331164dba0e6a/raw/732879093c15e23371b13297cc0f4843fdc0811d/index.html /usr/share/nginx/html/web2/index.html
