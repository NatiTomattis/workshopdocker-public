# Ejercicios dia 2

## Dockerfile reference

Lista de intrucciones mas utiles:

| COMANDO    | USO                                                                   |
|------------|-----------------------------------------------------------------------|
| FROM       | Imagen base para construir el contenedor                              |
| COPY       | Copiar un archivo del contexto a la imagen                            |
| ADD        | Parecido a COPY pero con funciones extendidas                         |
| RUN        | Correr un comando en la imagen durante la contruccion                 |
| ENV        | Declarar variables de entorno                                         |
| WORKDIR    | Cambia el directorio donde se ejecutan los comandos                   |
| ENTRYPOINT | Script default que va a correr la imagen                              |
| CMD        | Parametros de ENTRYPOINT (se pueden sobreescribir durante docker run) |
| ONBUILD    | Cuando una imagen extienda de esta lo primero que se va a correr      |
| VOLUME     | Declarar un volumen, crea un volumen en el directorio por defecto     |
| EXPOSE     | Declarar que exponemos un puerto (no expone el puerto)                |

Para mas info, leer [Dockerfile Refernce](https://docs.docker.com/engine/reference/builder/)

## Comandos utiles para hoy 

**Comando para construir una imagen**

```bash
docker build -t [IMAGE_NAME] [BUILD_CONTEXT]
```
**Comando para borrar una imagen**

```bash
docker rmi [IMAGE_NAME]
```
**Comando inspeccionar capas de una imagen**

```bash
docker image inspect [IMAGE_NAME]
docker history [IMAGE_NAME]
```
## Ejercicio 1

FROM
LABEL
RUN

## Ejercicio 2

ENTRYPOINT
CMD

## Ejercicio 3

COPY
ADD

## Ejercicio 4

WORKDIR
EXPOSE
VOLUME

## Ejercicio 5: Multi stage build

Build de un contenedor en multiples etapas para disminuir el tamanio de la imagen

## Ejercicio 6: publicacion de imagenes

- Push de imagenes
- Automated build
