# Publicar una imagen en Docker Hub

Existen 2 formas de publicar una imagen:

## Push de la imagen

```bash
docker login --username=usuario --email=mi@email
docker tag NOMBRE_IMAGEN usuario/mi_imagen:0.0.1
docker push usuario/mi_imagen:0.0.1
```

## Build Automatizado

1. Crear una cuenta en docker y github/bitbucket
2. Crear un repositorio en github/bitbucket
3. Desde docker hub crear un nuevo build automatizado y elegir el repositorio del que acabamos de crear
4. Elegir un nombre para el build (este es el nombre que va a tomar nuestra imagen una vez que este lista) y agregar una breve descripcion
5. En Build Settings, configurar el directorio donde se encuentra el dockerfile.
6. En Build Details podemos ver como docker hub comenzo a construir nuestra imagen